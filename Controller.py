from InteractionFuser import InteractionFuser
from Filter import Filter
from Fuser import Fuser


class Controller:

    def __init__(self):
        """
        This function creates firstly the trainset and saves this in
        Outputfiles/, after that it continues onto the testset and puts that,
        with the same format, in Outputfiles
        """
        toets = False
        self.filter = Filter('Test_files/Genes_relation.data.txt', toets)
        self.filter.load_rows_data(self.filter.groups)
        self.filter.writeData()

        iteracties = InteractionFuser("Test_files/Interactions_relation.data.txt",  self.filter, "out/Databeessies.train")
        Fuser(self.filter.data_frame['header'], 'out/Databeessies.train', "out/Databeessies.train",
                iteracties.relations, toets, "out/Databeessies.train.linked.csv")

        toets = True
        self.filter = Filter('Toets_files/Genes_relation.toets', True)
        self.filter.load_rows_data(self.filter.groups)
        self.filter.writeData()
        self.merge_set()
        iteracties = InteractionFuser("Toets_files/Interactions_relation.toets",  self.filter, "out/Databeessies.merged.csv")
        Fuser(self.filter.data_frame['header'], 'out/Databeessies.toets', "out/Databeessies.merged.csv",
                 iteracties.relations, toets, "out/Databeessies.toets.linked.csv")

    def merge_set(self):
        """
        This function opens the test and train set and merges it into a single
        file (.merged.csv)
        @return: None
        """
        filenames = ['Databeessies.toets', 'Databeessies.train']
        with open('out/Databeessies.merged.csv', 'w') as outfile:
            for fname in filenames:
                with open('out/'+fname) as infile:
                    for line in infile:
                        outfile.write(line)


if __name__ == '__main__':
    Controller()
