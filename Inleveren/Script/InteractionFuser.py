from operator import itemgetter


class InteractionFuser:
    def __init__(self, file, Filter, controlfile):
        """
        This function sets the loading in motion. It receives the filepath,
        Filter class and controlfile. These are used in the process
        :param file: string, path to file with gene data
        :param Filter: class, Filter class
        :param controlfile: string, path to file to check if relation is
        possible
        """
        self.filter = Filter
        self.relationData = self.loadData(file)
        linked = self.transformrelationData()
        self.relations = self.select_highscore(linked, controlfile)

    def loadData(self, fileName):
        """
        This functions opens the file given in fileName and returns this
        @param fileName: string, path to file
        @return: list, all lines in given file
        """
        with open(fileName, "r") as file:
            data = file.readlines()
            file.close()
            return data

    def select_highscore(self, linked, validationfile):
        """
        This function collects the highest scores from the geneIDs and their
        relations. For every geneID the highest score gets fetched and checked,
        if it exceeds the current highscore for that gene, it overwrites with
        the new score
        @param linked:
        @param validationfile:
        @return: dict, containing the highest corr scores
        """
        geneset = self.make_geneset(validationfile)
        highscores = {}
        for id, link in linked.items():
            ordered = sorted(link, key=itemgetter(3), reverse=True)
            highest = self.validate_highest(geneset, ordered)
            if highest:
                highscores[id] = highest
        return highscores

    def make_geneset(self, validationfile):
        """
        This function picks every line from the validationfile and appends it
        to geneset, this gets returned
        @param validationfile: string, filepath to file with data
        @return: list, all data from file sperated by comma
        """
        geneset = []
        for line in open(validationfile, 'r').readlines()[1:]:
            item = line.split(',')
            geneset.append(item[0])
        return geneset

    def validate_highest(self, geneset, orderd):
        """
        This function validates the highest gene in the orderedDict
        (indirectly from transformrelationData)
        @param geneset: list, contains all geneIDs
        @param orderd: dict, contains relation data per geneID. Accessed
            through geneID
        @return: False if no relations found, else it returns the highest corr
        """
        found = False
        i = 0
        while found == False:
            if orderd[i][0] in geneset and orderd[i][3] <= 1:
                found = True
                return orderd[i]
            if i == (len(orderd) - 1):
                return False
            i += 1

    def classify_corr(self, corr):
        """
        This function defines the parameter corr to a category. Firstly it
        checks if it is positive or negative, secondly it chooses the strength
        of the correlation
        @param corr: string, correlation for two genes
        @return: type, string; strength, string
        """
        corr = float(corr)
        if corr < 0:
            type = 'Negative'
        else:
            type = 'Positive'

        if abs(corr) > 0.74:
            strength = 'Strong'
        elif abs(corr) > 0.49:
            strength = 'Medium'
        else:
            strength = 'Low'

        return type, strength

    def transformrelationData(self):
        """
        This function transforms the relation data per geneID into floats and
        usefull info like strength (in categories)
        @return: data, dictionary containing relation data
        """
        data = {}
        for line in range(len(self.relationData)):
            self.relationData[line] = self.relationData[line].replace(".\n",
                                                                      "")
            a = self.relationData[line].split(",")
            if a[0] != a[1] and a[3] != '?':

                type, strenght = self.classify_corr(a[3])
                first = [a[1], a[2], a[3], abs(float(a[3])), type, strenght]
                second = [a[0], a[2], a[3], abs(float(a[3])), type, strenght]
                if a[0] in data:
                    data[a[0]].append(first)
                else:
                    data[a[0]] = [first]
                if a[1] in data:
                    data[a[1]].append(second)
                else:
                    data[a[1]] = [first]
        return data


if __name__ == '__main__':
    interaction = InteractionFuser()
    print(interaction.geneSet)
    print(interaction.getBestHits())
