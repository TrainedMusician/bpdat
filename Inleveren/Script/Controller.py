from Inleveren.Script.Filter import Filter
from Inleveren.Script.Fuser import Fuser
from Inleveren.Script.InteractionFuser import InteractionFuser


class Controller:

    def __init__(self):

        toets = False
        self.filter = Filter(
            '../Inputfiles/Test_files/Genes_relation.data.txt', toets)
        self.filter.load_rows_data(self.filter.groups)
        self.filter.writeData()

        interacties = InteractionFuser(
            "../Inputfiles/Test_files/Interactions_relation.data.txt",
            self.filter, "../Outputfiles/Databeessies.train")
        Fuser(self.filter.data_frame['header'],
              '../Outputfiles/Databeessies.train',
              "../Outputfiles/Databeessies.train",
              interacties.relations, toets,
              "../Outputfiles/Databeessies.train.linked.csv")

        toets = True
        self.filter = Filter('../Inputfiles/Toets_files/Genes_relation.toets',
                             True)
        self.filter.load_rows_data(self.filter.groups)
        self.filter.writeData()
        self.merge_set()
        interacties = InteractionFuser(
            "../Inputfiles/Toets_files/Interactions_relation.toets",
            self.filter, "../Outputfiles/Databeessies.merged.csv")
        Fuser(self.filter.data_frame['header'],
              '../Outputfiles/Databeessies.toets',
              "../Outputfiles/Databeessies.merged.csv",
              interacties.relations, toets,
              "../Outputfiles/Databeessies.toets.linked.csv")

    def merge_set(self):
        filenames = ['Databeessies.toets', 'Databeessies.train']
        with open('../Outputfiles/Databeessies.merged.csv', 'w') as outfile:
            for fname in filenames:
                with open('../Outputfiles/' + fname) as infile:
                    for line in infile:
                        outfile.write(line)


if __name__ == '__main__':
    Controller()
