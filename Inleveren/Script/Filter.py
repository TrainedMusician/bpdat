import re


class Filter:

    def __init__(self, genes, toets):
        """
        This function saves the given parameters to itself and loads the data
        @param genes: string, path to file with gene data
        @param toets: boolean, test-situation or not
        """
        self.file = genes
        self.toets = toets
        self.load_init_data()

    def load_init_data(self):
        """
        This function loads the data into itself. It sets the headers in the
        correct order and saves these in self
        @return: None
        """
        self.classes = ['Essential', 'Class', 'Complex', 'Phenotype', 'Motif',
                        'Chromosome', 'Function', 'Localization']
        self.data_frame = {'header': [], 'groups': []}
        titles, groups, groupname = self.create_header(
            open('../Inputfiles/Test_files/Genes_relation.names.txt'))
        self.groups = groups
        self.groupnames = groupname
        self.data_frame['header'].append(titles)

    def load_rows_data(self, groups):
        """
        This function loads the rows with gene data.
        @param groups: list, all headers
        @return: None
        """
        loaded_data = self.load_data_rows(open(self.file), groups)
        self.data_frame['rows'] = self.boolean_masking(loaded_data, groups)

    def boolean_masking(self, loaded_data, groups):
        """
        This function masks the booleans per group. If a boolean in a group is
        set to True, the proper values for the others are set instead of "?"
        @param loaded_data: list, rows with gene data
        @param groups: list, headers
        @return: list, rows with proper values
        """
        results = []
        for item in loaded_data:

            if self.validate_data(item):
                founded = self.found_item(item)
                missing = self.handle_missing_values(item)
                results.append(self.create_bool_row(item
                                                    , founded, missing))
        return results

    def validate_data(self, item):
        """
        This function validates the data, depending on a maximum of 3 missing
        values
        @param item: list, geneID to check
        @return: boolean, whether to kick or not. If test situation is true it
        gets marked as true to keep the value
        """
        amount = 0
        for att in item:
            if (att[0] == '@'):
                amount += 1

        if self.toets:
            return True
        return amount < 4

    def create_bool_row(self, item, founded, missing):
        """
        This function creates from the question marks the correct booleans in
        a group
        @param item: list, data for one geneID
        @param founded: int, amount found
        @param missing: int, amount missing
        @return: list, new created group with proper values
        """
        result = []
        atts = self.data_frame['header'][0][
               1:len(self.data_frame['header'][0]) - 1]
        for i in range(len(atts)):
            if i in founded:
                output = '1'
            elif i in missing:
                output = '0'
            else:
                output = '0'
            result.append(output)
        result.insert(0, item[0])
        if self.toets:
            result.append('?')
        else:
            result.append(item[len(item) - 1])
        return result

    def found_item(self, item):
        """
        This function searches the corresponding header with the current value
        @param item: list, row from gene data
        @return: list, attributes; or None when an error happened
        """
        atts = self.data_frame['header'][0][1:]
        checked = []
        for subitem in item[1:]:
            try:
                index = atts.index(subitem)
                if index >= 0:
                    checked.append(index)
            except ValueError:
                pass
        return checked

    def handle_missing_values(self, item):
        """
        This function handles the group when there are missing values. All the
        values get filled with question marks
        @param item: list, row from gene data
        @return: list, row after the check for missing values
        """
        atts = self.data_frame['header'][0][1:]
        checked = []
        for subitem in item:
            if subitem[0] == '@':
                lword = subitem.split('_')
                lword = lword[len(lword) - 1]
                try:
                    index = self.classes.index(lword)
                    for attr in self.groupnames[index]:
                        index = atts.index(attr)
                        if index >= 0:
                            checked.append(index)
                except ValueError:
                    pass
        return checked

    def get_gene_ids(self, data):
        """
        This function fetches all the geneIDs from the parameter data
        @param data: list, gene data
        @return: list, unique geneIDs
        """
        gene_ids = []
        for line in data:
            gene_detail = self.__splitCorrectly(line)
            if len(gene_detail) > 1:
                gene_ids.append(gene_detail[0])
        return set(gene_ids)

    def filter_data(self, data):
        """
        This function adds the @no_ to a group if there is not a single value
        in that group present
        @param data: list, data of genes
        @return: list, data of genes
        """
        result = []
        for item in data:
            cleaned = item
            unkown = []
            for subitem in item:
                if subitem[0] == '@':
                    lword = subitem.split('_')
                    lword = lword[len(lword) - 1]
                    unkown.append(lword)
            for subitem in item:
                if subitem[0] != '@':
                    fword = subitem.split('_')[0]
                    if fword in unkown:
                        cleaned.remove('@no_' + fword)
            result.append(cleaned)

        return result

    def remove_duplicates(self, values):
        """
        This function removes duplicates from the given parameter
        @param values: list, may contain duplicate rows
        @return: list, does not contain duplicate rows
        """
        output = []
        seen = set()
        for value in values:
            if value not in seen:
                output.append(value)
                seen.add(value)
        return output

    def load_data_rows(self, file, groups):
        """
        This function loads the data into rows and checks for missing values
        @param file: list, rows to check and load
        @param groups: list, possible headers
        @return: list, loaded rows
        """
        rows = []
        results = []
        data = file.readlines()
        genes = self.get_gene_ids(data)
        c = 0
        for gene_id in genes:
            c += 1
            sub_genes = []
            for line in data:
                line = line.replace('"', "'")
                info_lines = self.__splitCorrectly(line)

                gene = []
                if len(info_lines) > 1:
                    if info_lines[0] == gene_id:
                        locations = []
                        for i in range(len(info_lines)):
                            if (i == 0):
                                gene_id = info_lines[i]
                                gene.append(gene_id)
                            elif (info_lines[i] != '?'):
                                attr = self.configure_attribute_name(
                                    info_lines[i])
                                for item in groups[i - 1]:
                                    if (item == attr):
                                        name = self.classes[i - 1] + '_' + attr
                                        gene.append(name)
                                    elif (item == 'Localization'):
                                        locations.append(attr)
                                        gene.append(attr)
                            else:
                                if self.toets and self.classes[
                                    i - 1] != 'Localization':
                                    gene.append('@no_' + self.classes[i - 1])
                                elif (not self.toets):
                                    gene.append('@no_' + self.classes[i - 1])

                        if gene:
                            sub_genes.append(gene)
            if sub_genes:
                flat_sub_genes = [j for sub in sub_genes for j in sub]
                flat_sub_genes = self.remove_duplicates(flat_sub_genes)
                results.append(flat_sub_genes)
                if not self.toets:
                    flat_sub_genes.append(locations[0])
        return self.filter_data(results)

    def split_complex(self, complex):
        """
        This function splits the complex group on a comma
        @param complex: string, complex
        @return: list, complex splitted
        """
        return complex.split(',')

    def __splitCorrectly(self, line):
        """
        This function splits the line on a comma only if this comma is not part
        of a function name (or between ")
        @param line: string, line from gene data
        @return: list, line correctly splitted
        """
        PATTERN = re.compile(r'''((?:[^,"']|"[^"]*"|'[^']*')+)''')
        return PATTERN.split(line.strip(".\n"))[1::2]

    @staticmethod
    def configure_attribute_name(attr):
        """
        This function transforms the attr into a clean version
        @param attr: string, string to transform
        @return: string, clean version of attr
        """
        subitem = re.sub('"', '', attr)
        subitem = re.sub("'", '', subitem)
        subitem = subitem.rstrip()
        subitem = re.sub('[\t\n.]', '', subitem)
        subitem = re.sub('\s+', ' ', subitem).strip()
        subitem = re.sub(' ', '_', subitem)
        subitem = re.sub(',', '', subitem)
        return subitem

    def create_header(self, file):
        """
        This function creates all the possible headers from the .names file
        @param file: string, path to .names file
        @return: titles, titles to recognize; groups, headers; groupnames,
        possible groups
        """
        titles = []
        groups = []
        groupnames = []
        data = file.readlines()
        data.pop(0)
        for line in data:
            parts = line.split(':')
            if (len(parts) > 1):
                main_title = parts[0]
                sub_parts = parts[1]
                group = []
                groupname = []
                if main_title != "Localization":
                    items = self.__splitCorrectly(sub_parts)
                    for subitem in items:
                        subitem = self.configure_attribute_name(subitem)
                        group.append(subitem)
                        groupname.append(main_title + '_' + subitem)
                        titles.append(main_title + '_' + subitem)
                    groupnames.append(groupname)
                    groups.append(group)
        titles.insert(0, 'GENEID')
        titles.append('Localization')
        groups.append(['Localization'])
        return titles, groups, groupnames

    def writeData(self):
        """
        This function writes the data into the outfile
        @return: None
        """
        if self.toets:
            fileIn = open("../Outputfiles/Databeessies.toets", "w")
        else:
            fileIn = open("../Outputfiles/Databeessies.train", "w")
        fileIn.write("%s\n" % (", ".join(self.data_frame['header'][0])))
        for row in self.data_frame['rows']:
            fileIn.write("%s\n" % (", ".join(row)))
        fileIn.close()


if __name__ == '__main__':
    Filter = Filter('Toets_files/Genes_relation.toets', True)
    Filter.load_rows_data(Filter.groups)
    Filter.writeData()
