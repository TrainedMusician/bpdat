import itertools

from Filter import Filter


class Fuser():

    def __init__(self, old_header, sourcefile, checkfile, interactions, toets,
                 outfile):
        """
        This function initalizes and ends with writing the data to the given
        outputfile
        @param old_header: list, current headers where relationheaders will
        be appended to
        @param sourcefile: string, path to sourcefile
        @param checkfile: string, path to checkfile
        @param interactions: dict, all relations per geneID
        @param toets: boolean, test-situation or not
        @param outfile: string, path to outfile
        """
        self.toets = toets
        genes = self.get_genes(sourcefile)
        trans_head = self.declare_outer_location()
        header = self.set_new_header(old_header, trans_head)
        options = self.get_options(checkfile)
        rows = self.append_gene_names(genes, interactions, options, trans_head)
        self.writeData(header, rows, outfile)

    def get_options(self, filename):
        """
        This function fetches all the possible genes to link to, given from
        the file corresponding to filename
        @param filename: string, path to file
        @return: dict, gene data, stripped from newline, splitted on comma
        """
        data = {}
        file = open(filename, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data[item[0]] = item
        return data

    def declare_outer_location(self):
        """
        This function opens the names file and loads all the possible
        locations, this is returned
        :return: list, containins all possible locations
        """
        for line in open('../Inputfiles/Test_files/Genes_relation.names.txt'):
            parts = line.split(':')
            if parts[0] == 'Localization':
                items = parts[1].split(',')
                nitems = []
                for item in items:
                    nitems.append(
                        'Localization_' + Filter.configure_attribute_name(
                            item))
        return nitems

    def set_new_header(self, old_header, trans_head):
        """
        This function adds the newly created headers. Since the best relation
        is added to the row, those new columns need to be added too
        :param old_header: list, contains the headers for one geneID
        :param trans_head: list, contains possible locations
        :return: list, containing the new columns (doubled in size)
        """
        columns_extra = ['Physical', 'Genetic', 'Genetic-Physical',
                         'Negative', 'Positive', 'Strong', 'Medium', 'Low']
        extra_fields = []
        for item in old_header[0]:
            extra_fields.append('relation_' + item)
        extra_fields = extra_fields[: -1]
        flat_header = list(
            itertools.chain(old_header[0], columns_extra, extra_fields[:-1],
                            trans_head))
        return flat_header

    def get_cor_rows(self, cor):
        """
        This function uses the class of the given parameter and sets the
        booleans correctly
        :param cor: list, information about correlation (strength)
        :return: list, booleans about cor
        """
        type = []
        if cor[1] == 'Physical':
            type.extend(['1', '0', '0'])
        elif cor[1] == 'Genetic':
            type.extend(['0', '1', '0'])
        else:
            type.extend(['0', '0', '1'])
        if cor[4] == 'Negative':
            type.extend(['0', '1'])
        else:
            type.extend(['1', '0'])
        if cor[5] == 'Strong':
            type.extend(['1', '0', '0'])
        elif cor[5] == 'Medium':
            type.extend(['0', '1', '0'])
        else:
            type.extend(['0', '0', '1'])
        return type

    def get_localisation(self, place, header):
        """
        This function fetches the correct location column to append the correct
        boolean to
        :param place: string, location where geneID belongs to
        :param header: list, all headers
        :return: list, booleans for the locations
        """
        index = []
        place = 'Localization_' + Filter.configure_attribute_name(place)
        for item in header:
            if item == place:
                index.append('1')
            else:
                index.append('0')
        return index

    def append_gene_names(self, genes, interactions, options, trans_head=None):
        """
        This function appends all the gene data in every row
        :param genes: list, available geneIDs with corresponding data
        :param interactions: dict, all available relations
        :param options: dict, all available geneIDs
        :param trans_head: list, all available locations
        :return: list, loaded gene data with booleans
        """
        rows = []
        amount = 0
        amount2 = 0
        if self.toets:
            extend = 7
        else:
            extend = 6
        for gene in genes[1:]:
            base_data = gene
            try:
                interaction = interactions[gene[0]]
                cor = self.get_cor_rows(interaction)
                extra_data = self.get_gene_by_gene(interaction[0], options)
                localisation = self.get_localisation(extra_data[-1],
                                                     trans_head)

                if self.toets:
                    extra_data = extra_data[:-1]
                else:
                    extra_data = extra_data[:-2]

                rows.append(list(
                    itertools.chain(base_data, cor, extra_data, localisation)))
                amount2 += 1
            except KeyError:
                rows.append(list(itertools.chain(base_data, list(
                    '0' * (len(gene) + extend)), list(
                    '0' * (len(trans_head))))))
        return rows

    def get_genes(self, sourcefile):
        """
        This function fetches all the genes from the given file and returns
        these
        @param sourcefile: string, path to gene data
        @return: list, all gene data
        """
        data = []
        file = open(sourcefile, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data.append(item)
        file.close()
        return data

    def get_gene_by_gene(self, target, genes):
        """
        This gene fetches the gene information for the given target
        @param target: string, geneID to search
        @param genes: dict, all genes
        @return: list, gene info for given geneID
        """
        return genes[target]

    def writeData(self, header, rows, outfile):
        """
        This function writes the data in the given outputfile
        @param header: list, headers to write
        @param rows: dict, information per geneID
        @param outfile: string, path to outputfile
        @return: None
        """
        fileIn = open(outfile, "w")
        fileIn.write("%s\n" % (", ".join(header)))
        for row in rows:
            if self.toets:
                fileIn.write("%s\n" % (", ".join(row[:-1])))
            else:
                fileIn.write("%s\n" % (", ".join(row)))
        fileIn.close()
