from Script.Filter import Filter
from Script.Fuser import Fuser
from Script.InteractionFuser import InteractionFuser


class Controller:

    def __init__(self):
        """
        This function creates firstly the trainset and saves this in
        Outputfiles/, after that it continues onto the testset and puts that,
        with the same format, in Outputfiles
        """
        toets = False
        self.filter = Filter('Inputfiles/Testfile/Genes_relation.data.txt',
                             toets)
        self.filter.load_rows_data(self.filter.groups)
        self.filter.writeData()

        interacties = InteractionFuser(
            "Inputfiles/Testfile/Interactions_relation.data.txt", self.filter,
            "Outputfiles/Databeessies.train")
        Fuser(self.filter.data_frame['header'],
              'Outputfiles/Databeessies.train',
              "Outputfiles/Databeessies.train",
              interacties.relations, toets,
              "Outputfiles/Databeessies.train.linked.csv")

        toets = True
        self.filter = Filter('Inputfiles/Toetsfile/Genes_relation.toets',
                             toets)
        self.filter.load_rows_data(self.filter.groups)
        self.filter.writeData()
        self.merge_set()
        interacties = InteractionFuser(
            "Inputfiles/Toetsfile/Interactions_relation.toets", self.filter,
            "Outputfiles/Databeessies.merged.csv")
        Fuser(self.filter.data_frame['header'],
              'Outputfiles/Databeessies.toets',
              "Outputfiles/Databeessies.merged.csv",
              interacties.relations, toets,
              "Outputfiles/Databeessies.toets.linked.csv")

    def merge_set(self):
        """
        This function opens the test and train set and merges it into a single
        file (.merged.csv)
        @return: None
        """
        filenames = ['Databeessies.toets', 'Databeessies.train']
        with open('Outputfiles/Databeessies.merged.csv', 'w') as outfile:
            for fname in filenames:
                with open('Outputfiles/' + fname) as infile:
                    for line in infile:
                        outfile.write(line)


if __name__ == '__main__':
    Controller()
