import operator

class InteractionFuser:
    def __init__(self, file, Filter):
        self.filter = Filter
        self.relationData = self.loadData(file)
        self.transformrelationData()
        #self.geneSet = self.visualizeRelationData()
        #self.relations = self.getBestHits()

    def loadData(self, fileName):
        with open(fileName, "r") as file:
            data = file.readlines()
            return data

    def transformrelationData(self):
        for line in range(len(self.relationData)):
            self.relationData[line] = self.relationData[line].replace(".\n", "")
            atributes = self.relationData[line].split(",")
            print(atributes )
            exit()
            for atribute in range(len(atributes)):
                self.relationData[line] = self.relationData[line].replace(self.relationData[line], "|".join(atributes))

    def visualizeRelationData(self):
        geneSet = {}


        for i in self.relationData:
            if (i.split("|")[0]) in geneSet:
                geneSet[(i.split("|")[0])] = geneSet[(i.split("|")[0])]+"|"+((i.split("|")[1])+"|"+(i.split("|")[2])+"|"
                                                                             + (i.split("|")[3]))
            else:
                geneSet[(i.split("|")[0])] = ((i.split("|")[1])+"|"+(i.split("|")[2])+"|"+(i.split("|")[3]))
            if (i.split("|")[1]) in geneSet:
                geneSet[(i.split("|")[1])] = geneSet[(i.split("|")[1])]+"|"+((i.split("|")[0])+"|"+(i.split("|")[2])+"|"
                                                                             + (i.split("|")[3]))
            else:
                geneSet[((i.split("|")[1]))] = ((i.split("|")[0])+"|"+(i.split("|")[2])+"|"+(i.split("|")[3]))
        return geneSet

    def getBestHits(self):
        # loop through every geneID and then through every hit to get the best hit
        # modify to check if best hit is possible (and not deleted because of ?'s and then sort by highest
        bestHits = {}
        ids = []
        for item in self.filter.data_frame['rows']:
            ids.append(item[0])
        print(self.relationData)
        exit()
        for geneID in self.geneSet:
            hitDict = {}
            for hit in range(2, len(self.geneSet[geneID].split("|")), 3):
                if self.geneSet[geneID].split("|")[hit] != "?" and self.geneSet[geneID].split("|")[hit] != "" and self.geneSet[geneID].split("|")[hit-2] in ids:
                    print(self.geneSet[geneID].split("|")[hit-2])
                    hitDict[self.geneSet[geneID].split("|")[hit-2]] = float(self.geneSet[geneID].split("|")[hit])
            if len(hitDict) == 0:
                continue
            index = max(hitDict.items(), key=operator.itemgetter(1))[0]
            bestHits[geneID] = [index, self.geneSet[geneID].split("|")[self.geneSet[geneID].split("|").index(index)+1], str(hitDict[index])]
        return bestHits

if __name__ == '__main__':
    interaction = InteractionFuser()
    print(interaction.geneSet)
    print(interaction.getBestHits())
