from collections import defaultdict
import re


class Filter:

    def __init__(self, genes, toets):
        self.file = genes
        self.toets = toets
        self.load_init_data()

    def load_init_data(self):
        self.classes = [ 'Class', 'Complex', 'Motif', 'Localization']
        self.data_frame = {'header': [], 'groups': []}
        titles, groups, groupname = self.create_header(open('Test_files/Genes_relation.names.txt'))
        self.groups = groups
        self.groupnames = groupname
        self.data_frame['header'].append(titles)



    def load_rows_data(self, groups):
        loaded_data = self.load_data_rows(open(self.file), groups)
        self.data_frame['rows'] = self.boolean_masking(loaded_data, groups)


    def boolean_masking(self, loaded_data, groups):
        results = []
        for item in loaded_data:
            if self.validate_data(item):
                founded = self.found_item(item)
                missing = self.handle_missing_values(item)
                results.append(self.create_bool_row(item
                                                    ,founded, missing))
        return results

    def validate_data(self, item):
        amount = 0
        for att in item:
            if (att[0] == '@'):
                amount+=1
        return  amount < 3

    def create_bool_row(self, item, founded, missing):
        result = []
        atts = self.data_frame['header'][0][1:len(self.data_frame['header'][0])-1]
        for i in range(len(atts)):
            if i in founded:
                output = '1'
            elif i in missing:
                output = '?'
            else:
                output = '0'
            result.append(output)
        result.insert(0, item[0])
        if self.toets:
            result.append('?')
        else:
            result.append(item[len(item)-1])
        return result


    def found_item(self, item):
        atts = self.data_frame['header'][0][1:]
        checked = []
        for subitem in item[1:]:
            try:
                index = atts.index(subitem)
                if index >= 0:
                    checked.append(index)
            except ValueError:pass
        return checked

    def handle_missing_values(self, item):
        atts = self.data_frame['header'][0][1:]
        checked = []
        for subitem in item:
            if subitem[0] == '@':
                lword = subitem.split('_')
                lword = lword[len(lword) - 1]
                try:
                    index = self.classes.index(lword)
                    for attr in self.groupnames[index]:
                        index = atts.index(attr)
                        if index >= 0:
                            checked.append(index)
                except ValueError:pass
        return checked

    def get_gene_ids(self, data):
        gene_ids = []
        for line in data:
            gene_detail = self.__splitCorrectly(line)
            if len(gene_detail) > 1:
                gene_ids.append(gene_detail[0])
        return set(gene_ids)

    def filter_data(self, data):
        result = []
        for item in data:
            cleaned = item
            unkown = []
            for subitem in item:
                if subitem[0] == '@':
                    lword = subitem.split('_')
                    lword = lword[len(lword)-1]
                    unkown.append(lword)
            for subitem in item:
                if subitem[0] != '@':
                    fword = subitem.split('_')[0]
                    if fword in unkown:
                        cleaned.remove('@no_'+fword)
            result.append(cleaned)

        return result


    def remove_duplicates(self, values):
        output = []
        seen = set()
        for value in values:
            if value not in seen:
                output.append(value)
                seen.add(value)
        return output


    def load_data_rows(self, file, groups):
        rows = []
        results = []
        data = file.readlines()
        genes = self.get_gene_ids(data)
        c = 0
        for gene_id in genes:
            c+=1
            sub_genes = []
            for line in data:
                line = line.replace('"', "'")
                info_lines = self.__splitCorrectly(line)
                gene = []
                if len(info_lines) > 1:
                    if info_lines[0] == gene_id:
                        atts = []
                        for item in info_lines:
                            atts.append(self.configure_attribute_name(item))

                        for clas in self.classes:
                            if clas != 'Localization':
                                res = self.find_type(clas, atts)
                                if res[0] != '@':
                                    res = clas+'_'+res
                                gene.append(res)

                        if gene:
                            gene.insert(0, gene_id)
                            sub_genes.append(gene)
                            locations = atts[-1]
            if sub_genes:
                flat_sub_genes = [j for sub in sub_genes for j in sub]
                flat_sub_genes = self.remove_duplicates(flat_sub_genes)
                results.append(flat_sub_genes)
                if not self.toets:
                    flat_sub_genes.append(locations)
        return self.filter_data(results)

    def find_type(self, clas, items):
        index = self.classes.index(clas)
        for item in items:
            if item in self.groups[index]:
                return item
        return '@no_'+clas

    def split_complex(self, complex):
        return complex.split(',')

    def __splitCorrectly(self, line):
        PATTERN = re.compile(r'''((?:[^,"']|"[^"]*"|'[^']*')+)''')
        return PATTERN.split(line.strip(".\n"))[1::2]

    def configure_attribute_name(self, attr):
        subitem = re.sub('"', '', attr)
        subitem = re.sub("'", '', subitem)
        subitem = subitem.rstrip()
        subitem = re.sub('[\t\n.]', '', subitem)
        subitem = re.sub('\s+', ' ', subitem).strip()
        subitem = re.sub(' ', '_', subitem)
        subitem = re.sub(',', '', subitem)
        return subitem


    def create_header(self, file):
        titles = []
        groups = []
        groupnames = []
        data = file.readlines()
        data.pop(0)
        for line in data:
            parts = line.split(':')
            if (len(parts) > 1):
                main_title = parts[0]
                sub_parts = parts[1]
                group = []
                groupname = []
                if main_title not in ["Localization", "Function", "Phenotype", "Chromosome", "Essential"]:
                    items = self.__splitCorrectly(sub_parts)
                    for subitem in items:
                        subitem = self.configure_attribute_name(subitem)
                        group.append(subitem)
                        groupname.append(main_title +'_'+ subitem)
                        titles.append(main_title +'_'+ subitem)
                    groupnames.append(groupname)
                    groups.append(group)
        titles.insert(0, 'GENEID')
        titles.append('Localization')
        groups.append(['Localization'])
        return titles, groups, groupnames

    def writeData(self):
        if self.toets:
            fileIn = open("out/Databeessies.toets", "w")
        else:
            fileIn = open("out/Databeessies.train", "w")
        fileIn.write("%s\n" % (", ".join(self.data_frame['header'][0])))
        for row in self.data_frame['rows']:
            fileIn.write("%s\n" % (", ".join(row)))
        fileIn.close()


if __name__ == '__main__':
    Filter = Filter('Toets_files/Genes_relation.toets', True)
    Filter.load_rows_data(Filter.groups)
    Filter.writeData()
