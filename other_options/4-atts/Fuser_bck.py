import itertools


class Fuser():

    def __init__(self , old_header, sourcefile, checkfile, interactions, toets):
        self.toets = toets
        genes = self.get_genes(sourcefile)
        header = self.set_new_header(old_header)
        options = self.get_options(checkfile)
        self.append_gene_names(genes, interactions, options)
        #rows = self.arrange_rows(relations, genes)
        # self.writeData(header, rows)

    def get_options(self, filename):
        data = {}
        file = open(filename, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data[item[0]] = item
        return data

    def set_new_header(self, old_header):
        columns_extra = ['Rtype', 'Expression_correlation']
        extra_fields = []
        for item in old_header[0]:
            extra_fields.append('relation_'+item)
        flat_header = list(itertools.chain(old_header[0], columns_extra, extra_fields))
        return flat_header

    def append_gene_names(self, genes, interactions, options):
        rows= []
        for gene in genes[1:]:
            base_data = gene
            try:
                interaction = interactions[gene[0]]
                #cor_data = [interaction[2], interaction[3]]
                #extra_data = self.get_gene_by_gene(interaction[1], options)
                #rows.append(list(itertools.chain(base_data, cor_data, extra_data)))
                # rows.append(list(itertools.chain(base_data, connection_data)))
            except KeyError:
                print("fout")
                #len(gene)+1
        print(rows)
        exit()

    def filler(self, amount):

        x = []
        for i in range(amount):
            x.append('?')
        return x

    def get_genes(self, sourcefile):
        data = []
        file = open(sourcefile, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data.append(item)
        return data

    def arrange_rows(self, relations, genes):
        rows = []
        for gene, gene_data in relations.items():
            base_data = genes[gene]
            cor_data = gene_data[1:]
            # extra_data = self.get_gene_by_gene(gene_data[0], genes)
            # rows.append(list(itertools.chain(base_data, cor_data, extra_data)))
            rows.append(list(itertools.chain(base_data, cor_data)))
        return rows

    def get_gene_by_gene(self, target, genes):
        return genes[target]

    def writeData(self, header, rows):
        fileIn = open("out/merged.csv", "w")
        fileIn.write("%s\n" % (", ".join(header)))
        for row in rows:
            fileIn.write("%s\n" % (", ".join(row)))
        fileIn.close()