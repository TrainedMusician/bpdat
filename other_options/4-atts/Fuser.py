import itertools


class Fuser():

    def __init__(self , old_header, sourcefile, checkfile, interactions, toets, outfile):
        self.toets = toets
        genes = self.get_genes(sourcefile)
        header = self.set_new_header(old_header)
        options = self.get_options(checkfile)
        rows = self.append_gene_names(genes, interactions, options)
        self.writeData(header, rows, outfile)

    def get_options(self, filename):
        data = {}
        file = open(filename, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data[item[0]] = item
        return data

    def set_new_header(self, old_header):
        columns_extra = ['Rtype', 'Expression_correlation', 'corr_type', 'corr_strength']
        extra_fields = []
        for item in old_header[0]:
            extra_fields.append('relation_'+item)
        flat_header = list(itertools.chain(old_header[0], columns_extra, extra_fields))
        return flat_header

    def append_gene_names(self, genes, interactions, options):
        rows= []
        amount = 0
        amount2 = 0
        for gene in genes[1:]:
            base_data = gene
            try:
                interaction = interactions[gene[0]]
                cor = [interaction[1], interaction[2], interaction[4], interaction[5]]
                extra_data = self.get_gene_by_gene(interaction[0], options)
                rows.append(list(itertools.chain(base_data, cor, extra_data)))
                amount2+=1
            except KeyError:
                rows.append(list(itertools.chain(base_data, ['?'], list('?'*(len(gene)+3)))))
        return rows

    def get_genes(self, sourcefile):
        data = []
        file = open(sourcefile, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data.append(item)
        return data

    def get_gene_by_gene(self, target, genes):
        return genes[target]

    def writeData(self, header, rows, outfile):
        fileIn = open(outfile, "w")
        fileIn.write("%s\n" % (", ".join(header)))
        for row in rows:
            if self.toets:
                fileIn.write("%s\n" % (", ".join(row[:-1])))
            else:
                fileIn.write("%s\n" % (", ".join(row)))
        fileIn.close()