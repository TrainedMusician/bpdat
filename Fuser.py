import itertools


class Fuser():

    def __init__(self, old_header, sourcefile, checkfile, interactions, toets,
                 outfile):
        """
        This function initalizes and ends with writing the data to the given
        outputfile
        @param old_header: list, current headers where relationheaders will
        be appended to
        @param sourcefile: string, path to sourcefile
        @param checkfile: string, path to checkfile
        @param interactions: dict, all relations per geneID
        @param toets: boolean, test-situation or not
        @param outfile: string, path to outfile
        """
        self.toets = toets
        genes = self.get_genes(sourcefile)
        header = self.set_new_header(old_header)
        options = self.get_options(checkfile)
        rows = self.append_gene_names(genes, interactions, options)
        self.writeData(header, rows, outfile)

    def get_options(self, filename):
        """
        This function fetches all the possible genes to link to, given from
        the file corresponding to filename
        @param filename: string, path to file
        @return: dict, gene data, stripped from newline, splitted on comma
        """
        data = {}
        file = open(filename, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data[item[0]] = item
        file.close()
        return data

    def set_new_header(self, old_header):
        """
        This function appends the relation headers to the already existing
        headers. These new set of headers get returned
        @param old_header: list, current headers
        @return: list, new headers (amount is doubled)
        """
        columns_extra = ['Rtype', 'Expression_correlation', 'corr_type',
                         'corr_strength']
        extra_fields = []
        for item in old_header[0]:
            extra_fields.append('relation_' + item)
        flat_header = list(
            itertools.chain(old_header[0], columns_extra, extra_fields))
        return flat_header

    def append_gene_names(self, genes, interactions, options):
        """
        This function appends all the gene data to a list, which later gets
        written to the outputfile. In this list the relationsdata is added
        @param genes: list, full file of the gene data
        @param interactions: list, possible interactions per geneID
        @param options: list, all possible genes
        @return: list, containing gene data per geneID. Includes relation data
        """
        rows = []
        amount = 0
        amount2 = 0
        for gene in genes[1:]:
            base_data = gene
            try:
                interaction = interactions[gene[0]]
                cor = [interaction[1], interaction[2], interaction[4],
                       interaction[5]]
                extra_data = self.get_gene_by_gene(interaction[0], options)
                rows.append(list(itertools.chain(base_data, cor, extra_data)))
                amount2 += 1
            except KeyError:
                rows.append(list(itertools.chain(base_data, ['?'],
                                                 list('?' * (len(gene) + 1)))))
        return rows

    def get_genes(self, sourcefile):
        """
        This function fetches all the genes from the given file and returns
        these
        @param sourcefile: string, path to gene data
        @return: list, all gene data
        """
        data = []
        file = open(sourcefile, "r")
        for row in file.readlines():
            item = row.strip("\n").split(',')
            data.append(item)
        file.close()
        return data

    def get_gene_by_gene(self, target, genes):
        """
        This gene fetches the gene information for the given target
        @param target: string, geneID to search
        @param genes: dict, all genes
        @return: list, gene info for given geneID
        """
        return genes[target]

    def writeData(self, header, rows, outfile):
        """
        This function writes the data in the given outputfile
        @param header: list, headers to write
        @param rows: dict, information per geneID
        @param outfile: string, path to outputfile
        @return: None
        """
        fileIn = open(outfile, "w")
        fileIn.write("%s\n" % (", ".join(header)))
        for row in rows:
            if self.toets:
                fileIn.write("%s\n" % (", ".join(row[:-1])))
            else:
                fileIn.write("%s\n" % (", ".join(row)))
        fileIn.close()
